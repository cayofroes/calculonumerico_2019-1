#include<iostream>
#include<string>
#include<cmath>
#include<vector>
#include <io.h>
#include <fcntl.h>

using namespace std;

//intervalo de iteracao
struct Intervalo
{
	double a;
	double b;
};

//tabela contendo as informacoes de todas iteracoes
struct Tabela
{
	double intA;
	double intB;
	double xIntervalo;
	double funcaoXIntervalo;
};

Intervalo intervalo;
vector<Tabela> tabela;

void recebeIntervalo(double a, double b);
double xAproximado(double a, double b);
void metodoNewton(Intervalo intervalo, double epsilon);
double funcao(double x);
double primeiraDerivada(double x);
double segundaDerivada(double x);

int main()
{
	recebeIntervalo(1.0f, 2.0f);
	metodoNewton(intervalo, 0.000001f);
	return 0;
}

void recebeIntervalo(double a, double b)
{
	intervalo.a = a;
	intervalo.b = b;
}

//funcao que retorna um ponto com base numa aproximacao de derivada
double xAproximado(double x, double xF, double xPD)
{
	return (x - xF/xPD);
}


//metodo da Secante
void metodoNewton(Intervalo intervalo, double epsilon)
{
  double x0;
  double xF;
  double xPD;
  double xSD;
  while(true){
    cin>>x0;
    xF = funcao(x0);
    xPD = primeiraDerivada(x0);
    xSD = segundaDerivada(x0);
    if((xPD!=0 && xSD!=0)){
      if(( xF*xSD > 0)){
        if(x0 >= intervalo.a && x0 <= intervalo.b){
          cout<<"parou"<<endl;
          break;
        }
      }
    }
    else{
      _setmode(_fileno(stdout), _O_U16TEXT);
      wcout<<"N�o atende �s condi��es suficiente.\nInsira um outro x0."<<endl;
    }
  }
  int k = 0;
  double xk = xAproximado(x0, xF, xPD);
  cout<<"xk = "<<xk<<endl;
  double x = x0;
  if(!(abs(xk-x)<= epsilon)){
    while(true){
      xF = funcao(xk);
      xPD = primeiraDerivada(xk);
      xSD = segundaDerivada(xk);
      x = xk;
      xk = xAproximado(xk, xF, xPD);
      cout<<"xk = "<<xk<<endl;
      k++;
      if(abs(xk-x)<= epsilon){
        break;
      }
    }
  }
  cout<<k<<endl;
}

double funcao(double x){
  return x*x*x*x + x - 4;
}

double primeiraDerivada(double x){
  return 4*x*x*x+1;
}

double segundaDerivada(double x){
  return 12*x*x;
}
