#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>

using namespace std;
//CONSTANTES
const int NEWTON = 1;
const int SECANTE = 2;
const int ARREDONDAMENTO = 3;
const int TRUNCAMENTO = 4;

//STRUCT QUE DESCREVE UM NÚMERO
struct Numero{
    double origin;
    double mantissa;
    int expoente;
    bool valid;
};

//MAQUINA UTILIZADA NO CÁLCULO
struct Maquina{
    int metodo;
    int base;
    int mantissa;
    int limiteinferior;
    int limitesuperior;
    int aproximacao;
    Numero a;
    Numero b;
    double epsilon;
};

Maquina maquina;

//FUNÇÃO QUE IMPRIME CENTRALIZADO UM TEXTO
void imprimeCentralizado(string s){
    int tamanho = (int) (80- s.size())/2;
    for(int i=0;i<tamanho;i++){
        cout<<" ";
    }
    cout<<s;
    cout<<endl;
}
//FUNÇÃO QUE IMPRIME UMA LINHA TRACEJADA
void imprimeLinhaTracejada(){
    for(int i=0;i<86;i++){
        cout<<"-";
    }
    cout<<endl;
}
//FUNÇÃO QUE CONVERTE UM NÚMERO QUALQUER EM UM NÚMERO DA MÁQUINA
//NO CASO, ARREDONDA OU APROXIMA E SETA A MANTISSA
//CONVERTE NÚMERO PARA FICAR ENTRE 0 E 1, DESLOCANDO A VÍRGULA E
//INCREMENTANDO O EXPOENTE, POR FIM, PEGA CADA ELEMENTO DENTRO DAS VÁLIDAS NA MANTISSA
//E ADICIONA A UM NOVO NÚMERO. POR FIM, DESLOCA O NÚMERO NOVAMENTE PARA SUA POSIÇÃO CORRETA
//PRESERVANDO SUA MANTISSA E O ARREDONDAMENTO E TRUNCAMENTO
Numero convert2maquina(Numero n){
    n.valid = true;
    double number = n.origin;
    n.expoente = 0;
    n.origin = 0;
    double new_number=0;
    while(abs(number)>1){
        number/=10;
        n.expoente+=1;
        if(n.expoente>maquina.limitesuperior){
            n.valid = false;
            break;
        }
        if(n.valid == false){
            n.expoente=0;
            n.mantissa=0;
            n.origin=0;
            n.valid=false;
            return n;
        }
    }
    if(maquina.aproximacao == TRUNCAMENTO){
        for(int i=1;i<=maquina.mantissa;i++){
            number*=10;
            int int_number = number;
            number-=int_number;
            new_number+= int_number*pow(10,-(i));
        }
    }else{
        for(int i=1;i<=maquina.mantissa;i++){
            number*=10;
            int int_number = number;
            number-=int_number;
            new_number+= int_number*pow(10,-(i));
        }
        if(number>0.5){
            new_number+=1*pow(10,-(maquina.mantissa));
        }
    }
    n.mantissa = new_number;
    n.origin = n.mantissa;
    for(int i=0;i<n.expoente;i++){
        n.origin*=10;
    }
    return n;
    
}
//FUNÇÃO UTILIZADA
double funcao(double variavel)
{
	return sin(variavel) +cos(variavel)+1;
}
//PRIMEIRA DERIVADA
double derivada1funcao(double variavel){
    return cos(variavel) - sin(variavel);
}
//SEGUNDA DERIVADA
double derivada2funcao(double variavel){
    return -sin(variavel) - cos(variavel);
}
//BUSCA O PRÓXIMO NÚMERO
Numero next_number(){
    Numero n;
    if(maquina.metodo == SECANTE)
        n.origin = (maquina.b.origin - ((funcao(maquina.b.origin) * (maquina.b.origin-maquina.a.origin))/(funcao(maquina.b.origin) - funcao(maquina.a.origin))));
    else{
        n.origin = (maquina.a.origin - (funcao(maquina.a.origin)/derivada1funcao(maquina.a.origin)));
    }
    return n;
}

//METODO DA SECANTE
void secante(){
    Numero n = next_number();
    n = convert2maquina(n);
    int k=1;
    if(!n.valid){
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\tf(x)="<<funcao(n.origin)<<endl;
        return;    
    }
	double fx = funcao(n.origin);
	if(abs(fx) < maquina.epsilon){
        return;
	}
	while(abs(fx) >= maquina.epsilon){
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(n.origin)<<"\t"<<"[a:b]=["<<maquina.a.origin<<":"<<maquina.b.origin<<"]\tf(x) < epsilon?= "<<"NÃO"<<endl;
        k++;
		maquina.a = maquina.b;
		maquina.b = n;
		n = next_number();
        n = convert2maquina(n);
		fx = funcao(n.origin);
	}
	maquina.a = maquina.b;
	maquina.b = n;
	n = next_number();
	fx = funcao(n.origin);
    cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(n.origin)<<"\t"<<"[a:b]=["<<maquina.a.origin<<":"<<n.origin<<"]\tf(x) < epsilon?= "<<"SIM"<<endl;
        
}
//METODO DE NEWTON
void newton(double x0){
    Numero n;
    n.origin = x0;
    //n = convert2maquina(n);
    maquina.a = n;
    int k=1;
    double fx1d = derivada1funcao(maquina.a.origin);
    double fx2d = derivada2funcao(maquina.a.origin);
    n = next_number();
    n = convert2maquina(n);
    if(!n.valid){
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(maquina.a.origin)<<"\t"<<"(xk+1)-(xk)="<<abs(maquina.a.origin-n.origin)<<endl;
        return;    
    }
    cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(maquina.a.origin)<<"\t"<<"(xk+1)-(xk)="<<abs(maquina.a.origin-n.origin)<<endl;
    while(!(abs(maquina.a.origin-n.origin)<=maquina.epsilon)){
        maquina.a = n;
        maquina.a = convert2maquina(maquina.a);
        if(!n.valid){
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(maquina.a.origin)<<"\t"<<"(xk+1)-(xk)="<<abs(maquina.a.origin-n.origin)<<endl;
        return;    
    }
        n = next_number();
        n = convert2maquina(n);
        if(!n.valid){
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(maquina.a.origin)<<"\t"<<"(xk+1)-(xk)="<<abs(maquina.a.origin-n.origin)<<endl;
        return;    
    }
        k++;
        cout<<fixed<<setprecision(maquina.mantissa+1)<<"i="<<k<<"\t"<<"x="<<n.origin<<"\t"<<"f(x)="<<funcao(maquina.a.origin)<<"\t"<<"(xk+1)-(xk)="<<abs(maquina.a.origin-n.origin)<<endl;
    
    }

}
//VERIFICA SE 3 VALORES DO INTERVALO SÃO VÁLIDOS
//O INICIAL, O FINAL E O DO MEIO. CASO SEJA ERRADO, ENCERRA O MÉTODO
void newton_criterio(){
    double x = maquina.a.origin;
    double fx = funcao(x);
    double fx1d = derivada1funcao(x);
    double fx2d = derivada2funcao(x);
    if((fx1d!=0 && fx2d!=0) && ( fx*fx2d > 0)){
        newton(x);

    }
    x = maquina.b.origin;
    fx = funcao(x);
    fx1d = derivada1funcao(x);
    fx2d = derivada2funcao(x);
    if((fx1d!=0 && fx2d!=0) && ( fx*fx2d > 0)){
        newton(x);
    }
    x = (maquina.a.origin + maquina.b.origin)/2;
    fx = funcao(x);
    fx1d = derivada1funcao(x);
    fx2d = derivada2funcao(x);
    if((fx1d!=0 && fx2d!=0) && ( fx*fx2d > 0)){
        newton(x);
    }

}
//FUNÇÃO QUE LÊ OS DADOS DA ENTRADA
void leituraDeDados(){
    
    //método
    imprimeLinhaTracejada();
    imprimeCentralizado("Escolha um método");
    imprimeCentralizado("[a] Método de Newton");
    imprimeCentralizado("[b] Método das Secantes");
    
    cout<<"Sua opção: ";
    string entrada;
    while(cin>>entrada && (entrada != "a" && entrada != "b")){
        cout<<"Opção inválida"<<endl;
        cout<<"Sua opção: ";
    }
    if(entrada == "a"){
        maquina.metodo = NEWTON;
    }else{
        maquina.metodo = SECANTE;
    }

    //base
    imprimeLinhaTracejada();
    imprimeCentralizado("Sua máquina será calculada na base b=10...");
    maquina.base=10;

    //mantissa e expoente
    imprimeLinhaTracejada();
    imprimeCentralizado("Definiremos agora os limites da sua máquina...");
    cout<<endl;
    cout<<"Qual o limite inferior do expoente da sua máquina? ";
    cin>>maquina.limiteinferior;
    cout<<"Qual o limite superior do expoente da sua máquina? ";
    cin>>maquina.limitesuperior;
    cout<<"Qual mantissa do seu numero? ";
    cin>>maquina.mantissa;

    //arredondamento ou truncamento?
    imprimeLinhaTracejada();
    imprimeCentralizado("Escolha uma aproximacao");
    imprimeCentralizado("[a] Arredondamento");
    imprimeCentralizado("[b] Truncamento");
    
    cout<<"Sua opção: ";
    while(cin>>entrada && (entrada != "a" && entrada != "b")){
        cout<<"Opção inválida"<<endl;
        cout<<"Sua opção: ";
    }
    if(entrada == "a"){
        maquina.aproximacao = ARREDONDAMENTO;
    }else{
        maquina.aproximacao = TRUNCAMENTO;
    }

    //a,b e epsilon
    imprimeLinhaTracejada();
    imprimeCentralizado("Definiremos agora o start do procedimento");
    cout<<endl;
    cout<<"Informe o valor de a: ";
    cin>>maquina.a.origin;
    //maquina.a = convert2maquina(maquina.a);
    cout<<"Informe o valor de b: ";
    cin>>maquina.b.origin;
    //maquina.b = convert2maquina(maquina.b);
    cout<<"Informe o valor de epsilon: ";
    cin>>maquina.epsilon;
    imprimeLinhaTracejada();
    system("clear||cls");

}
//FUNÇÃO PARA IMPRIMIR INFORMAÇÕES DE INÍCIO
void informacaoDosDados(){
    imprimeLinhaTracejada();
    imprimeCentralizado("MAQUINA START");
    imprimeLinhaTracejada();
    cout<<"MAQUINA START ";
    cout<<"base="<<maquina.base;
    cout<<" a="<<maquina.a.origin<<" b="<<maquina.b.origin;
    cout<<" epsilon="<<maquina.epsilon;
    cout<<endl;
    imprimeLinhaTracejada();
}

int main(){
    leituraDeDados();
    informacaoDosDados();
    if(maquina.metodo == SECANTE){
        secante();
    }else{
        newton_criterio();
    }
}