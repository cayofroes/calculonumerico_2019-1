#include<iostream>
#include<string>
#include<cmath>
#include<vector>
using namespace std;

//intervalo de iteracao
struct Intervalo
{
	double a;
	double b;
};

//tabela contendo as informacoes de todas iteracoes
struct Tabela
{
	double intA;
	double intB;
	double xIntervalo;
	double funcaoXIntervalo;
};

Intervalo intervalo;
vector<Tabela> tabela;

//funcao trabalhada
double funcao(double variavel)
{
	return pow(variavel, 3) - 9*variavel + 5;
}

void recebeIntervalo(double a, double b)
{
	intervalo.a = a;
	intervalo.b = b;
}

//funcao que retorna um ponto com base numa aproximacao de derivada
double xAproximado(double a, double b)
{
	return (b - ((funcao(b) * (b-a))/(funcao(b) - funcao(a))));
}

void preencheTabela(Intervalo intervalo)
{
	Tabela tabelaTemporaria;
	tabelaTemporaria.intA = intervalo.a;
	tabelaTemporaria.intB = intervalo.b;
	tabelaTemporaria.xIntervalo = xAproximado(tabelaTemporaria.intA, tabelaTemporaria.intB);
	tabelaTemporaria.funcaoXIntervalo = funcao(tabelaTemporaria.xIntervalo);

	tabela.push_back(tabelaTemporaria);
}

void imprimeTabela()
{
	for(int i=0; i<tabela.size(); i++)
	{
		cout << "Tabela[" << i << "] -------------------\n";
		cout << "   Intervalo A:" << tabela[i].intA << endl;
		cout << "   Intervalo B:" << tabela[i].intB << endl;
		cout << "   xIntervalo:" << tabela[i].xIntervalo << endl;
		cout << "   funcaoXIntervalo:" << tabela[i].funcaoXIntervalo << endl;
		cout << "-------------------------------------------\n";
	}
}

//metodo da Secante
void metodoSecante(Intervalo intervalo, double epsilon)
{
	double xVariavel = xAproximado(intervalo.a, intervalo.b);
	double funcaoXVariavel = funcao(xVariavel);
	preencheTabela(intervalo);
	if(abs(funcaoXVariavel) < epsilon)
	{
		return;
	}
	while(abs(funcaoXVariavel) >= epsilon)
	{
		intervalo.a = intervalo.b;
		intervalo.b = xVariavel;
		xVariavel = xAproximado(intervalo.a, intervalo.b);
		funcaoXVariavel = funcao(xVariavel);
		preencheTabela(intervalo);
	}
	intervalo.a = intervalo.b;
	intervalo.b = xVariavel;
	xVariavel = xAproximado(intervalo.a, intervalo.b);
	funcaoXVariavel = funcao(xVariavel);
	preencheTabela(intervalo);

	imprimeTabela();
}

int main()
{
	recebeIntervalo(2.0f, 4.0f);
	metodoSecante(intervalo, 0.000001f);
	return 0;
}